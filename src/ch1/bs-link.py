from bs4 import BeautifulSoup

# 解析したいHTML
html = """
<html><body>
<ul>
<li><a href="http://uta.pw">uta</a></li>
<li><a href="http://oto.chu.jp">oto</a></li>
</ul>
<h1 id="title">スクレイピングとは</h1>
<p id="body">Webページを解析すること。</p>
<p>任意の箇所を抽出すること。</p>
</body></html>
"""

# HTMLを解析する
soup = BeautifulSoup(html,'html.parser')
soup.f
# findメソッドで取り出す 任意の部分を抽出する
links = soup.find_all("a")

# title = soup.find(id="title")
# body = soup.find(id="body")
# リンク一覧を表示
for a in links:
    href = a.attrs['href']
    text = a.string
    print(text,">",href)
# 要素のテキストを表示する
# print(title.string)
# print(body.string)
