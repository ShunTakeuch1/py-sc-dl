import urllib.request
import urllib.parse

API = "http://api.aoikujira.com/zip/xml/get.php"

# パラメータをURLエンコードする 日本語を含む場合はURLエンコードが必須となる
values = {
    'fmt':'xml',
    'zn': '1500042'
}
params = urllib.parse.urlencode(values)

# リクエスト用のURLを生成
url = API + "?" + params
print("url=",url)

# Download
data = urllib.request.urlopen(url).read()
text = data.decode("utf-8")
print(text)
