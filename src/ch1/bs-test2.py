from bs4 import BeautifulSoup

# 解析したいHTML
html = """
<html><body>
<h1 id="title">スクレイピングとは</h1>
<p id="body">Webページを解析すること。</p>
<p>任意の箇所を抽出すること。</p>
</body></html>
"""

# HTMLを解析する
soup = BeautifulSoup(html,'html.parser')

# findメソッドで取り出す 任意の部分を抽出する
title = soup.find(id="title")
body = soup.find(id="body")

# 要素のテキストを表示する
print(title.string)
print(body.string)
